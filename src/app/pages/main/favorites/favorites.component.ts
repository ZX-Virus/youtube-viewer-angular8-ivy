import { Component } from '@angular/core';
import {YoutubeListsService} from "../../../shared/services/youtube.lists.service";

@Component({
  selector: 'main-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss'],
})
export class FavoritesComponent {
  constructor(
    public youtubeListsService: YoutubeListsService,
  ) { }
}
