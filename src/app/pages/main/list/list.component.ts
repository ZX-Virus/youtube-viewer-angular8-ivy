import {Component} from "@angular/core";
import {FormBuilder, FormGroup} from "@angular/forms";
import {YoutubeListsService} from "../../../shared/services/youtube.lists.service";

@Component({
  selector: "main-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"],
})
export class ListComponent {
  public reactiveSearchForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    public youtubeListsService: YoutubeListsService,

  ) {
    this.reactiveSearchForm = this.fb.group({
      searchText: [""],
    });
  }

  public get searchTextValue() {
    return this.reactiveSearchForm.get("searchText").value;
  }
}
