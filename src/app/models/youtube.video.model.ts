export interface IYoutubeVideoThumbnail {
  height: number;
  width: number;
  url: string;
}

export interface IYoutubeVideoThumbnails {
  default: IYoutubeVideoThumbnail;
  high: IYoutubeVideoThumbnail;
  medium: IYoutubeVideoThumbnail;
}

export interface IYoutubeVideoSnippet {
  channelId: string;
  channelTitle: string;
  description: string;
  title: string;
  thumbnails: IYoutubeVideoThumbnails;
}

export interface IYoutubeVideoItem {
  etag: string;
  videoId: string;
  inList: boolean;
  isFavorite: boolean;
  snippet: IYoutubeVideoSnippet;
}

export interface IYoutubeVideoPageInfo {
  resultsPerPage: number;
  totalResults: number;
}

export interface IYoutubeVideoPage {
  nextPageToken: string;
  prevPageToken: string;
  pageInfo: IYoutubeVideoPageInfo;
  items: IYoutubeVideoItem[];
}
