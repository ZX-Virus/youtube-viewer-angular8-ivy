import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {MatButtonModule, MatIconModule, MatTabsModule} from "@angular/material";

@NgModule({
  imports: [
    BrowserAnimationsModule,
  ],
  exports: [
    BrowserAnimationsModule,
    MatTabsModule,
    MatIconModule,
    MatButtonModule,
  ],
})

export class MaterialModule {
}
