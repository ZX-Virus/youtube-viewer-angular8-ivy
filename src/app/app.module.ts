import { NgModule } from '@angular/core';
import {MAT_CHECKBOX_CLICK_ACTION} from "@angular/material";
import { BrowserModule } from '@angular/platform-browser';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './modules/app-routing.module';
import {ListComponent} from "./pages/main/list/list.component";
import {YoutubeListsService} from "./shared/services/youtube.lists.service";
import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { MainPageComponent } from './pages/main/main.component';
import { YouTubeApiService } from './shared/services/youtube.api.service';
import { FavoritesComponent } from './pages/main/favorites/favorites.component';

@NgModule({
  declarations: [
    AppComponent,
    MainPageComponent,
    ListComponent,
    FavoritesComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    AppRoutingModule,
    SharedModule,
  ],
  providers: [
    {provide: MAT_CHECKBOX_CLICK_ACTION, useValue: 'check'},
    YouTubeApiService,
    YoutubeListsService,
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
