import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from "@angular/core";
import {IYoutubeVideoItem, IYoutubeVideoThumbnail} from "../../../models/youtube.video.model";

@Component({
  selector: "app-youtube-video",
  templateUrl: "./youtube-video.component.html",
  styleUrls: ["./youtube-video.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class YoutubeVideoComponent {
  @Input() public item: IYoutubeVideoItem;
  @Input() public showFavoriteBtn: boolean = false;
  @Input() public showRemoveBtn: boolean = false;
  @Output() public toggle: EventEmitter<any> = new EventEmitter();
  @Output() public remove: EventEmitter<any> = new EventEmitter();

  public getThumbnailStyles(thumbnail: IYoutubeVideoThumbnail) {
    return {
      height: thumbnail.height + "px",
      width: thumbnail.width + "px",
      backgroundImage: `url(${thumbnail.url})`,
    };
  }
}
