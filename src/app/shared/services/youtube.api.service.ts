import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";

import { environment } from "../../../environments/environment";
import {IYoutubeVideoItem, IYoutubeVideoPage, IYoutubeVideoSnippet} from "../../models/youtube.video.model";

@Injectable()
export class YouTubeApiService {
  public maxItemsPerPage: number = 50;

  constructor(private httpClient: HttpClient) {
  }

  public getSearch(searchText: string = null, pageToken: string = null): Observable<IYoutubeVideoPage> {
    let params = new HttpParams();

    params = params.set("key", environment.googleApiKey);
    params = params.set("part", "snippet");
    params = params.set("maxResults", this.maxItemsPerPage.toString(10));

    if (searchText) {
      params = params.set("q", searchText);
    }
    if (pageToken) {
      params = params.set("pageToken", pageToken);
    }

    return this.httpClient
      .get<any>(`https://www.googleapis.com/youtube/v3/search`, { params })
      .pipe(
        map((pageRequest: any) => {
          const returnPage: IYoutubeVideoPage = {
            nextPageToken: pageRequest.nextPageToken,
            prevPageToken: pageRequest.prevPageToken,
            pageInfo: null,
            items: [],
          };
          if (pageRequest.pageInfo) {
            returnPage.pageInfo = {
              resultsPerPage: pageRequest.pageInfo.resultsPerPage,
              totalResults: pageRequest.pageInfo.totalResults,
            };
          }
          if (pageRequest.items) {
            returnPage.items = pageRequest.items.map((item) => {
              const videoIdLocal: string = (item.id && item.id.videoId) ? item.id.videoId : null;
              let snippetLocal: IYoutubeVideoSnippet = null;
              if (item.snippet) {
                snippetLocal = {
                  channelId: item.snippet.channelId,
                  channelTitle: item.snippet.channelTitle,
                  description: item.snippet.description,
                  title: item.snippet.title,
                  thumbnails: null,
                };
                if (item.snippet.thumbnails) {
                  snippetLocal.thumbnails = {
                    default: item.snippet.thumbnails.default,
                    high: item.snippet.thumbnails.high,
                    medium: item.snippet.thumbnails.medium,
                  };
                }
              }
              return {
                etag: item.etag,
                videoId: videoIdLocal,
                snippet: snippetLocal,
              } as IYoutubeVideoItem;
            });
          }
          return returnPage;
        }),
      );
  }
}
