import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";
import {IYoutubeVideoItem, IYoutubeVideoPage} from "../../models/youtube.video.model";
import {YouTubeApiService} from "./youtube.api.service";

@Injectable()
export class YoutubeListsService {
  public page$: Observable<IYoutubeVideoPage>;

  constructor(
    private youTubeApiService: YouTubeApiService,
  ) {
    this.page$ = of({
      nextPageToken: null,
      prevPageToken: null,
      pageInfo: null,
      items: [],
    });
    this.nextPage();
  }

  public nextPage(pageToken?: string, searchText?: string) {
    this.page$ = this.youTubeApiService.getSearch( searchText, pageToken).pipe(
      map((videoPage) => {
        const favorites = this.loadFavoritesFromStore();
        favorites.forEach((item) => item.inList = false);
        videoPage.items.forEach((item) => {
          item.inList = true;
          const findItem = favorites.find((favItem) => item.etag === favItem.etag);
          item.isFavorite = !!findItem;
          if (findItem) {
            findItem.inList = true;
          }
        });
        favorites.forEach((item) => {
          if (!item.inList) {
            videoPage.items.push({...item});
          }
        });
        return videoPage;
      }),
    );
  }

  public toggleInFavoriteList(item: IYoutubeVideoItem, allItems: IYoutubeVideoItem[]) {
    item.isFavorite = !item.isFavorite;
    this.page$ = this.page$.pipe(
      map((page) => {
        return {
          ...page,
          items: allItems,
        };
      }),
    );
    const favoriteItems = allItems.filter((itm) => itm.isFavorite);
    localStorage.setItem("favorites", JSON.stringify(favoriteItems));
  }

  private loadFavoritesFromStore(): IYoutubeVideoItem[] {
    const getItemsStr = localStorage.getItem("favorites");
    let retItems;
    if (getItemsStr) {
      try {
        retItems = JSON.parse(getItemsStr);
      } finally {}
    }
    return retItems ? retItems : [];
  }
}
