import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MaterialModule } from "../modules/material.module";
import { YoutubeVideoComponent } from "./components/youtube-video/youtube-video.component";

const PIPES = [];

const SERVICES = [];

const COMPONENTS = [
  YoutubeVideoComponent,
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ...PIPES,
    ...COMPONENTS,
    ...SERVICES,
  ],
  declarations: [
    ...PIPES,
    ...SERVICES,
    ...COMPONENTS,
  ],
  entryComponents: [
  ],
})

export class SharedModule {
}
